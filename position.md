## Position

### static

任意 position: static; 的元素不会被特殊的定位。一个 static 元素表示它不会被“positioned”


### relative


### fixed

一个固定定位（position属性的值为fixed）元素会相对于视窗来定位，这意味着即便页面滚动，它还是会停留在相同的位置

